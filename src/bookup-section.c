/* bookup-section.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-section.h"

struct _BookupSection
{
  GObject parent_instance;
  unsigned int id;
  char *name;
  char *rgb;
  unsigned int parent_id;
  unsigned int order_num;
};

G_DEFINE_TYPE (BookupSection, bookup_section, G_TYPE_OBJECT)

enum
{
  PROP_ID = 1,
  PROP_NAME,
  PROP_RGB,
  PROP_PARENT_ID,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

static void
bookup_section_finalize (GObject *obj)
{
  BookupSection *self = BOOKUP_SECTION (obj);

  if (self->name)
    {
      g_free (self->name);
      self->name = NULL;
    }

  if (self->rgb)
    {
      g_free (self->rgb);
      self->rgb = NULL;
    }

  G_OBJECT_CLASS (bookup_section_parent_class)->finalize (obj);
}

static void
bookup_section_set_property (GObject *object,
                             unsigned int property_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
  BookupSection *section = BOOKUP_SECTION (object);

  switch (property_id)
    {
    case PROP_ID:
      section->id = g_value_get_uint (value);
      break;
    case PROP_NAME:
      g_free (section->name);
      section->name = g_value_dup_string (value);
      break;
    case PROP_RGB:
      g_free (section->rgb);
      section->rgb = g_value_dup_string (value);
      break;
    case PROP_PARENT_ID:
      section->parent_id = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bookup_section_get_property (GObject *object,
                             unsigned int property_id,
                             GValue *value,
                             GParamSpec *pspec)
{
  BookupSection *section = BOOKUP_SECTION (object);

  switch (property_id)
    {
    case PROP_ID:
      g_value_set_uint (value, section->id);
      break;
    case PROP_NAME:
      g_value_set_string (value, section->name);
      break;
    case PROP_RGB:
      g_value_set_string (value, section->rgb);
      break;
    case PROP_PARENT_ID:
      g_value_set_uint (value, section->parent_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bookup_section_class_init (BookupSectionClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = bookup_section_set_property;
  gobject_class->get_property = bookup_section_get_property;
  gobject_class->finalize = bookup_section_finalize;

  properties[PROP_ID] =
      g_param_spec_uint ("id",
                         "ID",
                         "The ID of the section",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  properties[PROP_NAME] =
      g_param_spec_string ("name",
                           "Name",
                           "The name of the section",
                           NULL,
                           G_PARAM_READWRITE);

  properties[PROP_RGB] =
      g_param_spec_string ("rgb",
                           "RGB",
                           "The RGB value of the section",
                           NULL,
                           G_PARAM_READWRITE);

  properties[PROP_PARENT_ID] =
      g_param_spec_uint ("parent_id",
                         "Parent ID",
                         "The Parent ID of the section",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_READWRITE);

  g_object_class_install_properties (gobject_class, N_PROPERTIES, properties);
}

static void
bookup_section_init (BookupSection *self)
{
  self->id = 0;
  self->name = g_strdup ("");
  self->rgb = g_strdup ("");
  self->parent_id = 0;
  self->order_num = 0;
}

// Setter functions
void
bookup_section_set_id (BookupSection *self, unsigned int id)
{
  self->id = id;
}

void
bookup_section_set_name (BookupSection *self, const char *name)
{
  if (self->name != NULL)
    g_free (self->name);

  self->name = g_strdup (name);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
}

void
bookup_section_set_rgb (BookupSection *self, const char *rgb)
{
  if (self->rgb != NULL)
    g_free (self->rgb);

  self->rgb = g_strdup (rgb);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_RGB]);
}

void
bookup_section_set_parent_id (BookupSection *self, unsigned int id)
{
  self->parent_id = id;
}

// Getters

unsigned int
bookup_section_get_id (BookupSection *self)
{
  return self->id;
}

char *
bookup_section_get_name (BookupSection *self)
{
  return self->name;
}

char *
bookup_section_get_rgb (BookupSection *self)
{
  return self->rgb;
}

unsigned int
bookup_section_get_parent_id (BookupSection *self)
{
  return self->parent_id;
}

BookupSection *
bookup_section_new ()
{
  BookupSection *self = g_object_new (BOOKUP_TYPE_SECTION, NULL);

  return self;
}
