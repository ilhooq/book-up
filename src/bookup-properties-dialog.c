/* bookup-properties-dialog.c
 *
 * Copyright 2024 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-properties-dialog.h"
#include "bookup-application.h"
#include "bookup-database.h"
#include "system-file-manager.h"

struct _BookupPropertiesDialog
{
  AdwWindow parent_instance;

  BookupDatabase *db;

  AdwActionRow *name;
  AdwActionRow *location;
  GtkLabel *sections;
  GtkLabel *pages;
};

G_DEFINE_TYPE (BookupPropertiesDialog, bookup_properties_dialog, ADW_TYPE_WINDOW)

static GFile *
get_file (BookupPropertiesDialog *self)
{
  return g_file_new_for_path (bookup_database_get_filename (self->db));
}

static void
win_close_cb (GtkWidget *widget,
              G_GNUC_UNUSED const char *action_name,
              G_GNUC_UNUSED GVariant *param)
{
  gtk_window_close (GTK_WINDOW (widget));
}

static void
open_folder_cb (GtkWidget *widget,
                G_GNUC_UNUSED const char *action_name,
                G_GNUC_UNUSED GVariant *param)
{
  BookupPropertiesDialog *self = (BookupPropertiesDialog *) widget;
  g_autoptr (GFile) file = NULL;

  g_assert (BOOKUP_IS_PROPERTIES_DIALOG (self));

  if ((file = get_file (self)))
    system_file_manager_show (file, NULL);
}

static void
bookup_properties_dialog_class_init (BookupPropertiesDialogClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-properties-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupPropertiesDialog, location);
  gtk_widget_class_bind_template_child (widget_class, BookupPropertiesDialog, name);
  gtk_widget_class_bind_template_child (widget_class, BookupPropertiesDialog, sections);
  gtk_widget_class_bind_template_child (widget_class, BookupPropertiesDialog, pages);

  gtk_widget_class_install_action (widget_class, "open-folder", NULL, open_folder_cb);
  gtk_widget_class_install_action (widget_class, "win.close", NULL, win_close_cb);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Escape, 0, "win.close", NULL);
}

static void
bookup_properties_dialog_init (BookupPropertiesDialog *self)
{
  BookupApplication *app;

  g_autoptr (GFile) file = NULL;
  g_autoptr (GFile) parent = NULL;
  g_autofree char *basename = NULL;
  g_autofree char *parentname = NULL;
  g_autofree char *pages = NULL;
  g_autofree char *sections = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  app = BOOKUP_APPLICATION (g_application_get_default ());
  self->db = bookup_application_get_database (app);

  file = get_file (self);
  basename = g_file_get_basename (file);
  parent = g_file_get_parent (file);
  parentname = g_file_get_path (parent);

  pages = g_strdup_printf ("%i",
                           bookup_database_count_rows (self->db, "SELECT * FROM page"));
  sections = g_strdup_printf ("%i",
                              bookup_database_count_rows (
                                  self->db, "SELECT * FROM section"));

  adw_action_row_set_subtitle (self->name, basename);
  adw_action_row_set_subtitle (self->location, parentname);
  gtk_label_set_label (self->pages, pages);
  gtk_label_set_label (self->sections, sections);
}

GtkWidget *
bookup_properties_dialog_new (GtkWindow *transient_for)
{
  g_return_val_if_fail (GTK_IS_WINDOW (transient_for), NULL);

  BookupPropertiesDialog *self = g_object_new (BOOKUP_TYPE_PROPERTIES_DIALOG,
                                               "transient-for", transient_for,
                                               NULL);
  return GTK_WIDGET (self);
}
