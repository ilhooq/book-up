/* bookup-markdown-editor.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_MARKDOWN_EDITOR (bookup_markdown_editor_get_type ())

G_DECLARE_FINAL_TYPE (BookupMarkdownEditor, bookup_markdown_editor, BOOKUP, MARKDOWN_EDITOR, GtkBox)

void
bookup_markdown_editor_load_content (BookupMarkdownEditor *self,
                                     const gchar *content);
void
bookup_markdown_editor_empty (BookupMarkdownEditor *self);

gchar *
bookup_markdown_editor_get_content (BookupMarkdownEditor *self);

gboolean
bookup_markdown_editor_content_changed (BookupMarkdownEditor *self);

void
bookup_markdown_editor_set_style_scheme (BookupMarkdownEditor *self,
                                         const char *style_scheme_id);

void
bookup_markdown_editor_insert_image (BookupMarkdownEditor *self, unsigned int id);

G_END_DECLS
