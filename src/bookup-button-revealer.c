/* bookup-button-revealer.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-button-revealer.h"

enum
{
  PROP_BUTTON = 1,
  PROP_CHILD,
  N_PROPERTIES
};

struct _BookupButtonRevealer
{
  GtkWidget parent_instance;
  GtkButton *button;
  GtkWidget *child;
  GtkRevealer *revealer;
};

G_DEFINE_FINAL_TYPE (BookupButtonRevealer, bookup_button_revealer, GTK_TYPE_WIDGET)

static GParamSpec *properties[N_PROPERTIES];

static void
finalize (GObject *obj)
{
  BookupButtonRevealer *self = BOOKUP_BUTTON_REVEALER (obj);

  GtkWidget *revealer = GTK_WIDGET (self->revealer);
  GtkWidget *button = GTK_WIDGET (self->button);

  g_clear_pointer (&button, gtk_widget_unparent);
  g_clear_pointer (&revealer, gtk_widget_unparent);

  G_OBJECT_CLASS (bookup_button_revealer_parent_class)->finalize (obj);
}

static void
leave_focus (BookupButtonRevealer *self)
{
  bookup_button_revealer_reveal (self, FALSE);
}

static gboolean
key_pressed_cb (
    BookupButtonRevealer *self,
    guint keyval,
    G_GNUC_UNUSED guint keycode,
    G_GNUC_UNUSED GdkModifierType state)
{
  if (keyval == GDK_KEY_Escape)
    {
      bookup_button_revealer_reveal (self, FALSE);
    }

  return FALSE;
}

static void
button_clicked_cb (BookupButtonRevealer *self)
{
  bookup_button_revealer_reveal (self, TRUE);
}

static void
set_button (BookupButtonRevealer *self, GtkButton *button)
{
  g_return_if_fail (GTK_IS_BUTTON (button));

  g_signal_connect_swapped (button, "clicked", G_CALLBACK (button_clicked_cb), self);
  gtk_widget_set_parent (GTK_WIDGET (button), GTK_WIDGET (self));
  self->button = button;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_BUTTON]);
}

static void
set_revealer_child (BookupButtonRevealer *self, gpointer *child)
{
  g_return_if_fail (GTK_IS_WIDGET (child));

  self->child = GTK_WIDGET (child);
  gtk_revealer_set_child (self->revealer, self->child);

  if (GTK_IS_ENTRY (self->child) || GTK_IS_SEARCH_ENTRY (self->child))
    {
      GtkEventController *key_controller = gtk_event_controller_key_new ();
      gtk_widget_add_controller (GTK_WIDGET (self->child), key_controller);
      g_signal_connect_swapped (key_controller, "key-pressed", G_CALLBACK (key_pressed_cb), self);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CHILD]);
}

void
bookup_button_revealer_reveal (BookupButtonRevealer *self, gboolean reveal)
{
  gtk_revealer_set_reveal_child (self->revealer, reveal);
  gtk_widget_set_visible (GTK_WIDGET (self->button), !reveal);
}

static void
set_property (GObject *object,
              unsigned int property_id,
              const GValue *value,
              GParamSpec *pspec)
{
  BookupButtonRevealer *self = BOOKUP_BUTTON_REVEALER (object);

  switch (property_id)
    {
    case PROP_BUTTON:
      set_button (self, g_value_get_object (value));
      break;
    case PROP_CHILD:
      set_revealer_child (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
get_property (GObject *object,
              unsigned int property_id,
              GValue *value,
              GParamSpec *pspec)
{
  BookupButtonRevealer *self = BOOKUP_BUTTON_REVEALER (object);

  switch (property_id)
    {
    case PROP_BUTTON:
      g_value_set_object (value, self->button);
      break;
    case PROP_CHILD:
      g_value_set_object (value, self->child);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bookup_button_revealer_class_init (BookupButtonRevealerClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->set_property = set_property;
  gobject_class->get_property = get_property;
  gobject_class->finalize = finalize;

  properties[PROP_BUTTON] =
      g_param_spec_object ("button", NULL, NULL,
                           GTK_TYPE_BUTTON,
                           G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);
  properties[PROP_CHILD] =
      g_param_spec_object ("child", NULL, NULL,
                           GTK_TYPE_WIDGET,
                           G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (gobject_class, N_PROPERTIES, properties);
}

static void
bookup_button_revealer_init (BookupButtonRevealer *self)
{
  GtkBoxLayout *layout;
  layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (self)));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (layout), GTK_ORIENTATION_HORIZONTAL);

  self->revealer = g_object_new (GTK_TYPE_REVEALER, NULL);
  gtk_widget_set_parent (GTK_WIDGET (self->revealer), GTK_WIDGET (self));
  gtk_revealer_set_transition_type (self->revealer, GTK_REVEALER_TRANSITION_TYPE_SWING_RIGHT);

  GtkEventController *focus_controller = gtk_event_controller_focus_new ();
  gtk_widget_add_controller (GTK_WIDGET (self->revealer), focus_controller);
  g_signal_connect_swapped (focus_controller, "leave", G_CALLBACK (leave_focus), self);
}
