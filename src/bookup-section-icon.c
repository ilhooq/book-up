/* bookup-section-icon.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-section-icon.h"

enum
{
  PROP_RGB = 1,
  N_PROPERTIES
};

struct _BookupSectionIcon
{
  GtkDrawingArea parent_instance;
  char *rgb;
};

G_DEFINE_FINAL_TYPE (BookupSectionIcon, bookup_section_icon, GTK_TYPE_DRAWING_AREA)

static GParamSpec *properties[N_PROPERTIES];

static void
bookup_section_icon_finalize (GObject *obj)
{
  BookupSectionIcon *self = BOOKUP_SECTION_ICON (obj);

  if (self->rgb)
    {
      g_free (self->rgb);
      self->rgb = NULL;
    }

  G_OBJECT_CLASS (bookup_section_icon_parent_class)->finalize (obj);
}

static void
bookup_section_icon_set_property (GObject *object,
                                  unsigned int property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  BookupSectionIcon *self = BOOKUP_SECTION_ICON (object);

  switch (property_id)
    {
    case PROP_RGB:
      g_free (self->rgb);
      self->rgb = g_value_dup_string (value);
      gtk_widget_queue_draw (GTK_WIDGET (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bookup_section_icon_get_property (GObject *object,
                                  unsigned int property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  BookupSectionIcon *self = BOOKUP_SECTION_ICON (object);

  switch (property_id)
    {
    case PROP_RGB:
      g_value_set_string (value, self->rgb);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
draw_icon (GtkDrawingArea *area,
           cairo_t *cr,
           int width,
           int height,
           G_GNUC_UNUSED gpointer user_data)
{
  BookupSectionIcon *self = BOOKUP_SECTION_ICON (area);
  GdkRGBA rgba;

  gdk_rgba_parse (&rgba, self->rgb);

  cairo_set_source_rgba (cr, rgba.red, rgba.green, rgba.blue, 0.5);
  cairo_translate (cr, (double) width / 2, (double) height / 2);
  cairo_arc (cr, 0, 0, ((double) width / 2) - 2, 0, 2 * G_PI);
  cairo_fill_preserve (cr);
  cairo_set_line_width (cr, 1.3);
  cairo_set_source_rgb (cr, rgba.red, rgba.green, rgba.blue);
  cairo_stroke (cr);
}

static void
bookup_section_icon_class_init (BookupSectionIconClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = bookup_section_icon_set_property;
  gobject_class->get_property = bookup_section_icon_get_property;
  gobject_class->finalize = bookup_section_icon_finalize;

  properties[PROP_RGB] =
      g_param_spec_string ("rgb",
                           "Rgb",
                           "The icon color",
                           NULL,
                           G_PARAM_READWRITE);

  g_object_class_install_properties (gobject_class, N_PROPERTIES, properties);
}

static void
bookup_section_icon_init (BookupSectionIcon *self)
{
  self->rgb = NULL;

  gtk_drawing_area_set_draw_func (GTK_DRAWING_AREA (self), draw_icon, NULL, NULL);
  gtk_widget_set_valign (GTK_WIDGET (self), GTK_ALIGN_CENTER);
}

