#include <gio/gio.h>
#include "bookup-database.h"
#include "bookup-section.h"
#include "utils.h"
#include "config.h"

#define N_SECTIONS 4

static char* sections[N_SECTIONS] = {
  "INSERT INTO section (`id`, `name`, rgb, parent_id, order_num) VALUES (1, 'Test1', 'rgba(0,0,0)', 0, 0)",
  "INSERT INTO section (`id`, `name`, rgb, parent_id, order_num) VALUES (2, 'Test2', 'rgba(0,0,0)', 0, 0)",
  "INSERT INTO section (`id`, `name`, rgb, parent_id, order_num) VALUES (3, 'Test3', 'rgba(0,0,0)', 0, 0)",
  "INSERT INTO section (`id`, `name`, rgb, parent_id, order_num) VALUES (4, 'Test4', 'rgba(0,0,0)', 3, 0)",
};

static BookupDatabase *
create_database()
{
  char *db_path = g_strdup_printf("%s/test.db", TEST_DATA_DIR);
  GFile *db_file;

  db_file = g_file_new_for_path (db_path);

  if (g_file_query_exists (db_file, NULL)) {
    g_file_delete (db_file, NULL, NULL);
  }

  BookupDatabase *db = bookup_database_new (
    db_path,
    "/org/gnome/gitlab/ilhooq/Bookup/create_tables.sql"
  );

  g_assert (BOOKUP_IS_DATABASE (db));

  for (int i=0; i < N_SECTIONS; i++) {
    char *sql = sections[i];
    bookup_database_exec(db, sql);
  }

  g_free (db_path);
  g_object_unref (db_file);

  return db;
}

static void
test_fetch_rows (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;

  g_assert_true (BOOKUP_IS_DATABASE(db));

  unsigned int n_items, i;
  BookupSection *section;
  GListModel *list = bookup_database_fetch_rows(db,
                                                "SELECT * FROM section",
                                                BOOKUP_TYPE_SECTION);
  n_items = g_list_model_get_n_items(list);

  g_assert_true (g_list_model_get_n_items(list) == N_SECTIONS);

  for (i = 0; i < n_items; i++)
    {
      section = g_list_model_get_item (list, i);
      g_assert_true (BOOKUP_IS_SECTION (section));
    }
}

static void
test_fetch_row (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;

  g_assert_true (BOOKUP_IS_DATABASE(db));

  GObject *section;
  section = bookup_database_fetch_row (db,
                                       "SELECT * FROM section WHERE id=1",
                                       BOOKUP_TYPE_SECTION);

  g_assert_true (BOOKUP_IS_SECTION (section));
}

static void
test_count (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;
  g_assert_true (BOOKUP_IS_DATABASE(db));
  int count = bookup_database_count_rows (db, "SELECT * FROM section");
  g_assert_true (count == 4);
}

static void
test_insert (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;

  g_assert_true (BOOKUP_IS_DATABASE(db));

  BookupSection *section = g_object_new (BOOKUP_TYPE_SECTION, NULL);

  bookup_section_set_name (section, "Test5");
  bookup_section_set_parent_id (section, 3);

  g_assert_true (bookup_database_save (db, "section", G_OBJECT(section)));
  g_assert_true (bookup_section_get_id (section) == 5);
}

static void
test_update (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;

  g_assert_true (BOOKUP_IS_DATABASE(db));

  BookupSection *section;
  GListModel *list = bookup_database_fetch_rows(db,
                                                "SELECT * FROM section WHERE id=5",
                                                BOOKUP_TYPE_SECTION);
  section = g_list_model_get_item (list, 0);

  g_assert_true (BOOKUP_IS_SECTION (section));

  bookup_section_set_rgb (section, "rgba(150,150,150)");
  bookup_section_set_name (section, "[\"text\"]'");

  g_assert_true (bookup_database_save (db, "section", G_OBJECT(section)));
}

static void
test_delete (gconstpointer data)
{
  BookupDatabase *db = (BookupDatabase*) data;
  g_assert_true (BOOKUP_IS_DATABASE(db));

  BookupSection *section;
  GListModel *list;

  list = bookup_database_fetch_rows (db,
                                    "SELECT * FROM section WHERE id=5",
                                    BOOKUP_TYPE_SECTION);

  section = g_list_model_get_item (list, 0);

  g_assert_true (BOOKUP_IS_SECTION (section));
  g_assert_true (bookup_database_delete (db, "section", G_OBJECT(section)));

  g_object_unref (list);

  list = bookup_database_fetch_rows (db,
                                    "SELECT * FROM section WHERE id=5",
                                    BOOKUP_TYPE_SECTION);

  g_assert_true (!g_list_model_get_n_items (list));
}

int
main (int argc, char *argv[])
{
  BookupDatabase *db = create_database();

  g_test_init (&argc, &argv, NULL);
  g_test_add_data_func ("/Database/fetch_rows", (gconstpointer) db, test_fetch_rows);
  g_test_add_data_func ("/Database/fetch_row", (gconstpointer) db, test_fetch_row);
  g_test_add_data_func ("/Database/count", (gconstpointer) db, test_count);
  g_test_add_data_func ("/Database/insert", (gconstpointer) db, test_insert);
  g_test_add_data_func ("/Database/update", (gconstpointer) db, test_update);
  g_test_add_data_func ("/Database/delete", (gconstpointer) db, test_delete);

  return g_test_run ();
}
