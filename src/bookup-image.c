/* bookup-image.c
 *
 * Copyright 2024 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "bookup-image.h"

struct _BookupImage
{
  GObject parent_instance;
  unsigned int id;
  guint8 *data;
  gsize data_size;
};

G_DEFINE_TYPE (BookupImage, bookup_image, G_TYPE_OBJECT)

enum
{
  PROP_ID = 1,
  PROP_DATA,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

static void
bookup_image_finalize (GObject *object)
{
  BookupImage *image = BOOKUP_IMAGE (object);

  if (image->data)
    g_free (image->data);

  G_OBJECT_CLASS (bookup_image_parent_class)->finalize (object);
}

static void
bookup_image_set_property (GObject *object,
                           unsigned int property_id,
                           const GValue *value,
                           GParamSpec *pspec)
{
  BookupImage *self = BOOKUP_IMAGE (object);

  switch (property_id)
    {
    case PROP_ID:
      self->id = g_value_get_uint (value);
      break;
    case PROP_DATA:
      if (self->data)
        g_free (self->data);

      gpointer data = g_value_get_pointer (value);

      if (data != NULL)
        {
          GBytes *bytes = (GBytes *) data;
          self->data_size = g_bytes_get_size (bytes);
          const guint8 *data_ptr = g_bytes_get_data (bytes, &self->data_size);
          self->data = g_memdup2 (data_ptr, self->data_size);
          g_bytes_unref (bytes);
        }

      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
bookup_image_class_init (BookupImageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->set_property = bookup_image_set_property;
  object_class->finalize = bookup_image_finalize;

  properties[PROP_ID] =
      g_param_spec_uint ("id",
                         "ID",
                         "The page ID",
                         0,
                         G_MAXINT,
                         0,
                         G_PARAM_WRITABLE);

  properties[PROP_DATA] =
      g_param_spec_pointer ("data",
                            "Data",
                            "The binary data of the image",
                            G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
bookup_image_init (BookupImage *image)
{
  image->data = NULL;
  image->data_size = 0;
}

unsigned int
bookup_image_get_id (BookupImage *image)
{
  g_return_val_if_fail (image != NULL, 0);
  return image->id;
}

const guint8 *
bookup_image_get_data (BookupImage *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  return self->data;
}

gsize
bookup_image_get_data_size (BookupImage *self)
{
  g_return_val_if_fail (self != NULL, 0);
  return self->data_size;
}
