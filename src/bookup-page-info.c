/* bookup-page-info.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-page-info.h"
#include "bookup-marshal.h"
#include "bookup-page.h"
#include <glib/gi18n.h>

enum
{
  ADD_TAG,
  N_SIGNALS
};

struct _BookupPageInfo
{
  GtkBox parent_instance;
  GtkLabel *created_date;
  GtkLabel *modified_date;
  GMenuModel *menu;
  GListModel *tags_list;
  GtkFlowBox *tags_box;
  GtkEntry *tag_entry;
  unsigned int selected_tag_id;
};

G_DEFINE_FINAL_TYPE (BookupPageInfo, bookup_page_info, GTK_TYPE_BOX)

static unsigned int signals[N_SIGNALS];

static void
on_unmap_tag_button (GtkWidget *widget, G_GNUC_UNUSED gpointer user_data)
{
  GtkWidget *first_child;

  g_return_if_fail (GTK_IS_LABEL (widget));

  first_child = gtk_widget_get_first_child (widget);

  if (GTK_IS_POPOVER (first_child))
    gtk_widget_unparent (first_child);
}

static void
tag_entry_activated_cb (BookupPageInfo *self, GtkEntry *entry)
{
  g_autofree char *text = NULL;
  text = g_strdup (gtk_editable_get_text (GTK_EDITABLE (entry)));
  gtk_editable_set_text (GTK_EDITABLE (entry), "");
  g_signal_emit (self, signals[ADD_TAG], 0, text);
}

static void
tag_activated_cb (BookupPageInfo *self, GtkFlowBoxChild *child)
{
  BookupPage *tag;
  GtkWidget *widget;
  GtkWidget *first_child;
  GtkPopover *popover;
  int idx;

  g_return_if_fail (BOOKUP_IS_PAGE_INFO (self));

  idx = gtk_flow_box_child_get_index (child);
  tag = g_list_model_get_item (self->tags_list, idx);
  self->selected_tag_id = bookup_page_get_id (tag);
  widget = gtk_flow_box_child_get_child (child);
  first_child = gtk_widget_get_first_child (widget);

  if (GTK_IS_POPOVER (first_child))
    {
      popover = GTK_POPOVER (first_child);
    }
  else
    {
      popover = g_object_new (GTK_TYPE_POPOVER_MENU,
                              "menu-model", self->menu,
                              "has-arrow", TRUE,
                              "position", GTK_POS_BOTTOM,
                              NULL);
      gtk_widget_set_parent (GTK_WIDGET (popover), widget);
    }

  gtk_popover_popup (popover);
}

static GtkWidget *
create_tag_button (gpointer item, gpointer user_data)
{
  BookupPageInfo *self;
  GtkWidget *widget;
  const char *name;

  g_assert (BOOKUP_IS_PAGE (item));

  self = BOOKUP_PAGE_INFO (user_data);
  name = bookup_page_get_name (BOOKUP_PAGE (item));
  widget = gtk_label_new (name);

  g_signal_connect (widget, "unmap", G_CALLBACK (on_unmap_tag_button), self);

  return widget;
}

void
bookup_page_info_set_model (BookupPageInfo *self, GListModel *model)
{
  self->tags_list = model;
  gtk_flow_box_bind_model (self->tags_box, self->tags_list, create_tag_button, self, NULL);
}

void
bookup_page_info_set_entry_tag_completion_model (
    BookupPageInfo *self, GListModel *model)
{
  unsigned int i, n_items;
  g_autoptr (GtkListStore) list_store = NULL;
  GtkEntryCompletion *completion;
  GtkTreeIter iter;

  g_assert (g_list_model_get_item_type (model) == BOOKUP_TYPE_PAGE);

  list_store = gtk_list_store_new (1, G_TYPE_STRING);
  n_items = g_list_model_get_n_items (model);

  for (i = 0; i < n_items; i++)
    {
      gtk_list_store_append (list_store, &iter);
      gtk_list_store_set (list_store, &iter,
                          0, bookup_page_get_name (g_list_model_get_item (model, i)),
                          -1);
    }

  completion = gtk_entry_completion_new ();
  gtk_entry_completion_set_model (completion, GTK_TREE_MODEL (list_store));
  gtk_entry_completion_set_text_column (completion, 0);
  gtk_entry_set_completion (self->tag_entry, completion);
}

void
bookup_page_info_set_page (BookupPageInfo *self, const BookupPage *page)
{
  unsigned int timestamp;
  g_autoptr (GDateTime) datetime = NULL;
  g_autofree gchar *created_date = NULL;
  g_autofree gchar *modified_date = NULL;

  timestamp = bookup_page_get_creation_date ((BookupPage *) page);
  datetime = g_date_time_new_from_unix_local (timestamp);
  created_date = g_date_time_format (datetime, "%x");

  timestamp = bookup_page_get_modification_date ((BookupPage *) page);
  datetime = g_date_time_new_from_unix_local (timestamp);
  modified_date = g_date_time_format (datetime, "%x");

  gtk_label_set_label (self->created_date,
                       g_strdup_printf ("%s %s", _("Created at:"), created_date));

  gtk_label_set_label (self->modified_date,
                       g_strdup_printf ("%s %s", _("Updated at:"), modified_date));
}

unsigned int
bookup_page_info_get_selected_tag_id (BookupPageInfo *self)
{
  return self->selected_tag_id;
}

void
bookup_page_info_remove_all (BookupPageInfo *self)
{
  self->selected_tag_id = 0;
  gtk_label_set_label (self->created_date, "");
  gtk_label_set_label (self->modified_date, "");
  g_list_store_remove_all (G_LIST_STORE (self->tags_list));
}

static void
bookup_page_info_class_init (BookupPageInfoClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-page-info.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupPageInfo, created_date);
  gtk_widget_class_bind_template_child (widget_class, BookupPageInfo, modified_date);
  gtk_widget_class_bind_template_child (widget_class, BookupPageInfo, tags_box);
  gtk_widget_class_bind_template_child (widget_class, BookupPageInfo, menu);
  gtk_widget_class_bind_template_child (widget_class, BookupPageInfo, tag_entry);
  gtk_widget_class_bind_template_callback (widget_class, tag_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, tag_entry_activated_cb);

  signals[ADD_TAG] = g_signal_new ("add-tag",
                                   G_TYPE_FROM_CLASS (klass),
                                   G_SIGNAL_RUN_LAST,
                                   0,
                                   NULL, NULL,
                                   bookup_marshal_VOID__STRING,
                                   G_TYPE_NONE,
                                   1, G_TYPE_STRING);

  g_signal_set_va_marshaller (signals[ADD_TAG],
                              G_TYPE_FROM_CLASS (klass),
                              bookup_marshal_VOID__STRINGv);
}

static void
bookup_page_info_init (BookupPageInfo *self)
{
  self->selected_tag_id = 0;
  gtk_widget_init_template (GTK_WIDGET (self));
}
