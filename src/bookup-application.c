/* bookup-application.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-application.h"
#include "bookup-database.h"
#include "bookup-window.h"
#include "config.h"
#include <glib/gi18n.h>
#include <gtksourceview/gtksource.h>

enum
{
  BOOK_LOADED,
  BOOK_CLOSED,
  N_SIGNALS
};

struct _BookupApplication
{
  AdwApplication parent_instance;
  GSettings *settings;
  BookupDatabase *db;
};

G_DEFINE_TYPE (BookupApplication, bookup_application, ADW_TYPE_APPLICATION)

static unsigned int signals[N_SIGNALS];

/**
 * Copied from :
 * https://gitlab.gnome.org/GNOME/gnome-text-editor
 * /-/blob/44.0/src/editor-application-actions.c?ref_type=tags
 */
static char *
get_system_information (BookupApplication *self)
{
  GString *str = g_string_new (NULL);
  GtkSettings *gtk_settings = gtk_settings_get_default ();
  g_autoptr (GSettingsSchema) schema = NULL;
  g_autofree char *theme_name = NULL;
  g_auto (GStrv) keys = NULL;

  g_object_get (gtk_settings, "gtk-theme-name", &theme_name, NULL);
  g_object_get (self->settings, "settings-schema", &schema, NULL);

  g_string_append_printf (str, "%s (%s)", APP_ID, PACKAGE_VERSION);
#if DEVELOPMENT_BUILD
  g_string_append_printf (str, " Development");
#endif
  g_string_append (str, "\n");

  g_string_append (str, "\n");
  if (g_file_test ("/.flatpak-info", G_FILE_TEST_EXISTS))
    g_string_append (str, "Flatpak: yes\n");
  g_string_append_printf (str,
                          "GLib: %d.%d.%d (%d.%d.%d)\n",
                          glib_major_version,
                          glib_minor_version,
                          glib_micro_version,
                          GLIB_MAJOR_VERSION,
                          GLIB_MINOR_VERSION,
                          GLIB_MICRO_VERSION);
  g_string_append_printf (str,
                          "GTK: %d.%d.%d (%d.%d.%d)\n",
                          gtk_get_major_version (),
                          gtk_get_minor_version (),
                          gtk_get_micro_version (),
                          GTK_MAJOR_VERSION,
                          GTK_MINOR_VERSION,
                          GTK_MICRO_VERSION);
  g_string_append_printf (str,
                          "GtkSourceView: %d.%d.%d (%d.%d.%d)\n",
                          gtk_source_get_major_version (),
                          gtk_source_get_minor_version (),
                          gtk_source_get_micro_version (),
                          GTK_SOURCE_MAJOR_VERSION,
                          GTK_SOURCE_MINOR_VERSION,
                          GTK_SOURCE_MICRO_VERSION);
  g_string_append_printf (str,
                          "Libadwaita: %d.%d.%d (%d.%d.%d)\n",
                          adw_get_major_version (),
                          adw_get_minor_version (),
                          adw_get_micro_version (),
                          ADW_MAJOR_VERSION,
                          ADW_MINOR_VERSION,
                          ADW_MICRO_VERSION);

  g_string_append (str, "\n");
  g_string_append_printf (str, "gtk-theme-name: %s\n", theme_name);
  g_string_append_printf (str, "GTK_THEME: %s\n", g_getenv ("GTK_THEME") ?: "unset");
  g_string_append_printf (str, "GdkDisplay: %s\n", G_OBJECT_TYPE_NAME (gdk_display_get_default ()));

  g_string_append (str, "\n");
  g_string_append_printf (str, "Documents Directory: %s\n", g_get_user_special_dir (G_USER_DIRECTORY_DOCUMENTS));

  g_string_append (str, "\n");
  keys = g_settings_schema_list_keys (schema);
  for (guint i = 0; keys[i]; i++)
    {
      g_autoptr (GSettingsSchemaKey) key = g_settings_schema_get_key (schema, keys[i]);
      g_autoptr (GVariant) default_value = g_settings_schema_key_get_default_value (key);
      g_autoptr (GVariant) user_value = g_settings_get_user_value (self->settings, keys[i]);
      g_autoptr (GVariant) value = g_settings_get_value (self->settings, keys[i]);
      g_autofree char *default_str = g_variant_print (default_value, TRUE);
      g_autofree char *value_str = NULL;

      g_string_append_printf (str, "%s %s = ", APP_ID, keys[i]);

      if (user_value != NULL && !g_variant_equal (default_value, user_value))
        {
          value_str = g_variant_print (user_value, TRUE);
          g_string_append_printf (str, "%s [default=%s]\n", value_str, default_str);
        }
      else
        {
          value_str = g_variant_print (value, TRUE);
          g_string_append_printf (str, "%s\n", value_str);
        }
    }

  return g_string_free (str, FALSE);
}

static gboolean
style_variant_to_color_scheme (GValue *value,
                               GVariant *variant,
                               G_GNUC_UNUSED gpointer user_data)
{
  const char *str = g_variant_get_string (variant, NULL);

  if (g_str_equal (str, "follow"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_DEFAULT);
  else if (g_str_equal (str, "dark"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_DARK);
  else
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_LIGHT);

  return TRUE;
}

BookupApplication *
bookup_application_new (const char *application_id,
                        GApplicationFlags flags)
{
  g_return_val_if_fail (application_id != NULL, NULL);

  return g_object_new (BOOKUP_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
close_book_cb (GSimpleAction *action,
               GVariant *parameter,
               gpointer user_data)
{
  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_APPLICATION (user_data));

  BookupApplication *self = user_data;

  g_settings_set_string (self->settings, "bookup-database", "");
  g_settings_set_int (self->settings, "last-page", 0);
  g_object_unref (self->db);
  self->db = NULL;

  g_simple_action_set_enabled (action, FALSE);

  g_signal_emit (self, signals[BOOK_CLOSED], 0);
}

static void
update_database_schema (BookupApplication *self)
{
  int count = 0;

  count = bookup_database_count_rows (
      self->db,
      "SELECT * FROM pragma_table_info('page') WHERE `name` = 'trash';");

  if (!count)
    {
      bookup_database_exec (
          self->db,
          "ALTER TABLE page ADD COLUMN trash NOT NULL DEFAULT 0");
    }
}

void
bookup_application_open_book (BookupApplication *self,
                              const char *filename,
                              const gboolean create_schema)
{
  g_return_if_fail (strlen (filename));

  const char *schema_path = "/org/gnome/gitlab/ilhooq/Bookup/create_tables.sql";
  GAction *action;

  if (self->db)
    g_object_unref (self->db);

  self->db = create_schema
                 ? bookup_database_new (filename, schema_path)
                 : bookup_database_new (filename, NULL);

  if (!create_schema)
    update_database_schema (self);

  g_settings_set_string (self->settings, "bookup-database", filename);

  if ((action = g_action_map_lookup_action (G_ACTION_MAP (self), "close-book")))
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);

  g_signal_emit (self, signals[BOOK_LOADED], 0);
}

GSettings *
bookup_application_get_settings (BookupApplication *app)
{
  return app->settings;
}

BookupDatabase *
bookup_application_get_database (BookupApplication *app)
{
  return app->db;
}

static void
bookup_application_startup (GApplication *app)
{
  /*
  AdwApplication handles library initialization by calling adw_init() in the default
  GApplication::startup signal handler, in turn chaining up as required by GtkApplication.
  Therefore, any subclass of AdwApplication should always chain up its
  startup handler before using any Adwaita or GTK API.
  */
  G_APPLICATION_CLASS (bookup_application_parent_class)->startup (app);

  /* Automatically load style.css */
  g_application_set_resource_base_path (app, "/org/gnome/gitlab/ilhooq/Bookup");
}

static void
bookup_application_shutdown (GApplication *app)
{
  BookupApplication *self = BOOKUP_APPLICATION (app);
  g_object_unref (self->db);

  G_APPLICATION_CLASS (bookup_application_parent_class)->shutdown (app);
}

static void
bookup_application_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (BOOKUP_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (BOOKUP_TYPE_WINDOW, "application", app, NULL);

  gtk_window_present (window);
}

static void
bookup_application_class_init (BookupApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);
  app_class->activate = bookup_application_activate;
  app_class->startup = bookup_application_startup;
  app_class->shutdown = bookup_application_shutdown;

  signals[BOOK_LOADED] = g_signal_new ("book-loaded",
                                       G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL, NULL,
                                       NULL,
                                       G_TYPE_NONE,
                                       0);

  signals[BOOK_CLOSED] = g_signal_new ("book-closed",
                                       G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_LAST,
                                       0,
                                       NULL, NULL,
                                       NULL,
                                       G_TYPE_NONE,
                                       0);
}

static void
bookup_application_about_action (GSimpleAction *action,
                                 GVariant *parameter,
                                 gpointer user_data)
{
  static const char *developers[] = { "Sylvain PHILIP", NULL };
  BookupApplication *self = user_data;
  GtkWindow *window = NULL;
  g_autofree char *system_information = NULL;

  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (BOOKUP_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));
  system_information = get_system_information (self);

  /* clang-format off */
  adw_show_about_dialog (GTK_WIDGET (window),
                         "application-name", _("Bookup"),
                         "application-icon", "org.gnome.gitlab.ilhooq.Bookup",
                         "developer-name", "Sylvain PHILIP",
                         "version", PACKAGE_VERSION,
                         "developers", developers,
                         "copyright", _("© 2023 Sylvain PHILIP"),
                         /*
                         TRANSLATORS: eg.
                         'Translator Name <your.email@domain.com>' or
                         'Translator Name https://website.example'
                         */
                         "translator-credits", _("translator-credits"),
                         "debug-info", system_information,
                         NULL);
  /* clang-format on */
}

static void
bookup_application_quit_action (GSimpleAction *action,
                                GVariant *parameter,
                                gpointer user_data)
{
  g_assert (G_IS_ACTION (action));
  g_assert (parameter == NULL);
  g_assert (G_IS_APPLICATION (user_data));

  g_application_quit (G_APPLICATION (user_data));
}

static const GActionEntry app_actions[] = {
  { .name = "quit", .activate = bookup_application_quit_action },
  { .name = "about", .activate = bookup_application_about_action },
  { .name = "close-book", .activate = close_book_cb },
};

static void
bookup_application_init (BookupApplication *self)
{
  AdwStyleManager *style_manager;
  GAction *action;

  self->db = NULL;
  self->settings = g_settings_new ("org.gnome.gitlab.ilhooq.bookup");
  style_manager = adw_style_manager_get_default ();

  g_settings_bind_with_mapping (self->settings, "style-variant",
                                style_manager, "color-scheme",
                                G_SETTINGS_BIND_GET,
                                style_variant_to_color_scheme,
                                NULL, NULL, NULL);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   app_actions,
                                   G_N_ELEMENTS (app_actions),
                                   self);
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]) { "<primary>q", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.close-book",
                                         (const char *[]) { "<primary>w", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.new-book",
                                         (const char *[]) { "<primary>n", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.open-book",
                                         (const char *[]) { "<primary>o", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.show-preferences",
                                         (const char *[]) { "<primary>comma", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.initiate-search",
                                         (const char *[]) { "<primary>f", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.toggle-show-edit",
                                         (const char *[]) { "<primary>space", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.toggle-sidebar",
                                         (const char *[]) { "F9", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.toggle-page-info",
                                         (const char *[]) { "<alt>Return", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.toggle-bookmark",
                                         (const char *[]) { "<primary>d", NULL });

  if ((action = g_action_map_lookup_action (G_ACTION_MAP (self), "close-book")))
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
}
