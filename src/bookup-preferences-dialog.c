/* bookup-preferences-dialog.c
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bookup-preferences-dialog.h"
#include "bookup-application.h"
#include <gtksourceview/gtksource.h>

struct _BookupPreferencesDialog
{
  AdwPreferencesDialog parent_instance;

  GSettings *settings;
  AdwComboRow *scheme_chooser;
  GtkFontDialogButton *editor_font_button;
  GtkSourceBuffer *buffer;
};

G_DEFINE_TYPE (BookupPreferencesDialog, bookup_preferences_dialog, ADW_TYPE_PREFERENCES_DIALOG)

const char *markdown_preview = "# Markdown\n"
                               " 1. Numbered Lists\n"
                               " - Unnumbered and [Links](https://www.gnome.org/)\n"
                               " - `Preformatted Text`\n"
                               " - _Emphasis_ or *Emphasis* **Combined**\n"
                               "> Block quotes too!";

static const char *
get_settings_default_value (BookupPreferencesDialog *self, const char *setting_key)
{
  g_autoptr (GSettingsSchema) schema = NULL;
  g_autoptr (GSettingsSchemaKey) key = NULL;
  g_autoptr (GVariant) default_value = NULL;

  g_object_get (self->settings, "settings-schema", &schema, NULL);
  key = g_settings_schema_get_key (schema, setting_key);
  default_value = g_settings_schema_key_get_default_value (key);

  return g_variant_get_string (default_value, 0);
}

static void
on_reset_editor_preferences (BookupPreferencesDialog *self)
{
  const char *style = get_settings_default_value (self, "style-scheme");
  const char *font = get_settings_default_value (self, "editor-font");
  PangoFontDescription *font_desc;
  GListModel *model;

  int i, count;

  model = adw_combo_row_get_model (self->scheme_chooser);

  count = g_list_model_get_n_items (model);

  for (i = 0; i < count; i++)
    {
      GtkSourceStyleScheme *scheme = g_list_model_get_item (model, i);

      if (g_str_equal (gtk_source_style_scheme_get_id (scheme), style))
        {
          adw_combo_row_set_selected (self->scheme_chooser, i);
          break;
        }
    }

  font_desc = pango_font_description_from_string (font);

  if (font_desc != NULL)
    {
      gtk_font_dialog_button_set_font_desc (self->editor_font_button, font_desc);
      pango_font_description_free (font_desc);
    }
}

static void
on_map_js_source_view (GtkSourceView *source_view, BookupPreferencesDialog *self)
{
  g_autofree char *js = NULL;
  GtkTextBuffer *buffer;
  GtkSourceLanguageManager *manager;
  GtkSourceLanguage *language;
  manager = gtk_source_language_manager_get_default ();
  language = gtk_source_language_manager_get_language (manager, "js");

  js = g_settings_get_string (self->settings, "custom-js");
  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (source_view));

  gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (buffer), language);
  gtk_source_buffer_set_highlight_syntax (GTK_SOURCE_BUFFER (buffer), TRUE);

  gtk_text_buffer_set_text (buffer, js, -1);
}

static void
on_change_custom_js (GtkSourceBuffer *buffer, BookupPreferencesDialog *self)
{
  GtkTextIter start, end;
  g_autofree char *js = NULL;

  gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buffer), &start);
  gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buffer), &end);

  js = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer),
                                 &start,
                                 &end,
                                 FALSE);

  g_settings_set_string (self->settings, "custom-js", js);
}

static void
on_map_css_source_view (GtkSourceView *source_view, BookupPreferencesDialog *self)
{
  g_autofree char *css = NULL;
  GtkTextBuffer *buffer;
  GtkSourceLanguageManager *manager;
  GtkSourceLanguage *language;
  manager = gtk_source_language_manager_get_default ();
  language = gtk_source_language_manager_get_language (manager, "css");

  css = g_settings_get_string (self->settings, "custom-css");
  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (source_view));

  gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (buffer), language);
  gtk_source_buffer_set_highlight_syntax (GTK_SOURCE_BUFFER (buffer), TRUE);

  gtk_text_buffer_set_text (buffer, css, -1);
}

static void
on_change_custom_css (GtkSourceBuffer *buffer, BookupPreferencesDialog *self)
{
  GtkTextIter start, end;
  g_autofree char *css = NULL;

  gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buffer), &start);
  gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buffer), &end);

  css = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer),
                                  &start,
                                  &end,
                                  FALSE);

  g_settings_set_string (self->settings, "custom-css", css);
}

static void
on_change_editor_style_scheme (BookupPreferencesDialog *self)
{
  GtkSourceStyleScheme *scheme;
  const char *scheme_id;

  g_assert (BOOKUP_IS_PREFERENCES_DIALOG (self));

  scheme = adw_combo_row_get_selected_item (self->scheme_chooser);
  gtk_source_buffer_set_style_scheme (self->buffer, scheme);

  scheme_id = gtk_source_style_scheme_get_id (scheme);

  g_settings_set_string (self->settings, "style-scheme", scheme_id);
}

static void
update_font (BookupPreferencesDialog *self)
{
  PangoFontDescription *font_desc;
  g_autofree char *font = NULL;

  g_assert (BOOKUP_IS_PREFERENCES_DIALOG (self));

  font = g_settings_get_string (self->settings, "editor-font");
  font_desc = pango_font_description_from_string (font);

  if (font_desc != NULL)
    {
      gtk_font_dialog_button_set_font_desc (self->editor_font_button, font_desc);
      pango_font_description_free (font_desc);
    }
}

static void
on_change_editor_font_desc (BookupPreferencesDialog *self)
{
  const PangoFontDescription *font_desc;
  g_autofree char *font = NULL;

  g_assert (BOOKUP_IS_PREFERENCES_DIALOG (self));

  font_desc = gtk_font_dialog_button_get_font_desc (self->editor_font_button);
  font = pango_font_description_to_string (font_desc);

  g_settings_set_string (self->settings, "editor-font", font);
}

static void
update_style_schemes (BookupPreferencesDialog *self)
{
  GtkSourceStyleSchemeManager *sm;
  const char *const *scheme_ids;
  g_autoptr (GListStore) schemes = NULL;
  g_autofree char *current_scheme_id = NULL;
  unsigned int pos = 0;

  g_assert (BOOKUP_IS_PREFERENCES_DIALOG (self));

  schemes = g_list_store_new (GTK_SOURCE_TYPE_STYLE_SCHEME);
  sm = gtk_source_style_scheme_manager_get_default ();
  scheme_ids = gtk_source_style_scheme_manager_get_scheme_ids (sm);

  if (!scheme_ids)
    return;

  current_scheme_id = g_settings_get_string (self->settings, "style-scheme");

  for (guint i = 0; scheme_ids[i]; i++)
    {
      GtkSourceStyleScheme *scheme;
      scheme = gtk_source_style_scheme_manager_get_scheme (sm, scheme_ids[i]);
      g_list_store_append (schemes, scheme);

      if (g_str_equal (current_scheme_id, gtk_source_style_scheme_get_id (scheme)))
        pos = i;
    }

  adw_combo_row_set_model (self->scheme_chooser, G_LIST_MODEL (schemes));
  adw_combo_row_set_selected (self->scheme_chooser, pos);
}

static void
editor_preferences_dialog_constructed (GObject *object)
{
  BookupPreferencesDialog *self = (BookupPreferencesDialog *) object;
  GtkSourceLanguageManager *manager;
  GtkSourceLanguage *language;

  G_OBJECT_CLASS (bookup_preferences_dialog_parent_class)->constructed (object);

  manager = gtk_source_language_manager_get_default ();
  language = gtk_source_language_manager_get_language (manager, "markdown");

  gtk_text_buffer_set_text (GTK_TEXT_BUFFER (self->buffer), markdown_preview, -1);
  gtk_source_buffer_set_language (self->buffer, language);
  gtk_source_buffer_set_highlight_syntax (self->buffer, TRUE);

  update_style_schemes (self);
  update_font (self);
}

static void
bookup_preferences_dialog_class_init (BookupPreferencesDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = editor_preferences_dialog_constructed;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/gitlab/ilhooq/Bookup/bookup-preferences-dialog.ui");
  gtk_widget_class_bind_template_child (widget_class, BookupPreferencesDialog, buffer);
  gtk_widget_class_bind_template_child (widget_class, BookupPreferencesDialog, scheme_chooser);
  gtk_widget_class_bind_template_child (widget_class, BookupPreferencesDialog, editor_font_button);
  gtk_widget_class_bind_template_callback (widget_class, on_change_editor_style_scheme);
  gtk_widget_class_bind_template_callback (widget_class, on_change_editor_font_desc);
  gtk_widget_class_bind_template_callback (widget_class, on_change_custom_css);
  gtk_widget_class_bind_template_callback (widget_class, on_map_css_source_view);
  gtk_widget_class_bind_template_callback (widget_class, on_reset_editor_preferences);
  gtk_widget_class_bind_template_callback (widget_class, on_change_custom_js);
  gtk_widget_class_bind_template_callback (widget_class, on_map_js_source_view);
}

static void
bookup_preferences_dialog_init (BookupPreferencesDialog *self)
{
  BookupApplication *app;

  gtk_widget_init_template (GTK_WIDGET (self));

  app = BOOKUP_APPLICATION (g_application_get_default ());
  self->settings = bookup_application_get_settings (app);
}
