/* bookup-sections-view.h
 *
 * Copyright 2023 Sylvain Philip <contact@sphilip.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include "bookup-section.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BOOKUP_TYPE_SECTIONS_VIEW (bookup_sections_view_get_type ())

G_DECLARE_FINAL_TYPE (BookupSectionsView, bookup_sections_view, BOOKUP, SECTIONS_VIEW, GtkWidget)

void
bookup_sections_view_set_model (BookupSectionsView *self, GListModel *list);

void
bookup_sections_view_select_section (BookupSectionsView *self,
                                     BookupSection *section);
void
bookup_sections_view_add (BookupSectionsView *self,
                          BookupSection *section);

void
bookup_sections_view_remove_all (BookupSectionsView *self);

BookupSection *
bookup_sections_view_get_selected (BookupSectionsView *self);

void
bookup_sections_view_remove_selected (BookupSectionsView *self);

gboolean
bookup_sections_view_on_drop (GtkDropTarget *drop_target,
                              const GValue *value,
                              double x,
                              double y,
                              BookupSectionsView *self);

G_END_DECLS
