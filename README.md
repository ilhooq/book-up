# Bookup: A lightweight note-taking application

Book-up is a simple and efficient note-taking application written in C and GTK4,
designed as a clone of [Notes-up](https://github.com/Philip-Scott/Notes-up).

Unlike Notes-up, Book-up doesn't have any Granite dependency,
making it ideal for non-Elementary OS users.

![UI](./screenshot/bookup-1.png)

## Features

- Organization of notes in books (which are sqlite databases).
- Arrangement of notes in tree form.
- Markdown syntax highlighting in edit mode.
- Sections and notes can be reorganized by dragging and dropping with the mouse.
- Assignment of tags to make it easier to find notes.
- Easy-to-use markdown editor.
- Export notes to PDF and Markdown formats.
- Ability to customize the editor and viewer appearance.

## Installation

### Flathub

Official package available on Flathub:

<a href="https://flathub.org/apps/org.gnome.gitlab.ilhooq.Bookup"><img width="240" alt="Download on Flathub" src="https://dl.flathub.org/assets/badges/flathub-badge-en.svg" /></a>

### Manual installation

To install Book-up, follow these steps:

System-wide:

```bash
meson setup build
ninja -C build
ninja -C build install
```

User-specific:

```bash
meson setup --prefix $HOME/.local build
ninja -C build
ninja -C build install
```
